function checkValidityElements(){
    var formContainer = document.getElementById('form_container');
    var firstelement = true;
    var flag = 1;
    $('input:not([type="submit"]):not([type="hidden"]), textarea').each(function() {
    if (this.checkValidity()) {
      this.nextElementSibling.innerHTML = '';
    }
      else {
        flag = 0;
        this.nextElementSibling.innerHTML = this.validationMessage;
      if (firstelement) {
        var parentTabPane = this.closest('.tab-pane');
        var tabLinkToActivate = formContainer.querySelector('a[href="#' + parentTabPane.id + '"]');
        tabLinkToActivate.click();
        firstelement = false;
      }
    }
  
  });
  return flag;
    }
  $('.amount').bind("input", function(input) {
    input.target.value = input.target.value.replace(/\D/g, '');
  })
  $('input[name="hra_rent"]').bind("input", function(input) {
    if(input.target.value > 100000)
    {
      var errormessage = "Landlord pan is required if amount is greater than Rs 100000";
      $(this).next('span.error').text(errormessage);
      $('input[name="landlord_pan"]').attr("required","required");
    }
  })
  jQuery('.pan').keyup(function() {
          $(this).val($(this).val().toUpperCase());
      });
   $('.btnNext').click(function(event){
    if(checkValidityElements()){
      $('.nav-tabs > .active').next('li').find('a').trigger('click');
    }
  });
  
    $('.btnPrevious').click(function(){
      if(checkValidityElements()){
    $('.nav-tabs > .active').prev('li').find('a').trigger('click');
      }
  });
  
      
      /**************************************************** Add Items (Clone) ****************************************************/
      $('form').on('click', '.addItem', function () {
  //increment the value of our counter
          var items = $(this).parents('.items'),
                  item = items.find('.item'),
                  index_no = item.length + 1;
                  (index_no);
          //clone the first .item element
          var newItem = item.first().clone();                                         
          //recursively set our id, name, and for attributes properly
          newItem.find('.salNewNo').text(index_no);
          newItem.find('select').val('');
          newItem.find('.error-message').remove();
          newItem.find('.alert').remove();
          (newItem[0]);
          // Remember, the recursive function expects to be able to pass in
          // one parameter, the element.
          childRecursive(newItem, function (e) {
              setCloneAttr(e, index_no);
          });
          // Clear the values recursively
          childRecursive(newItem, function (e) {
              clearCloneValues(e);
          });
          // Finally, add the new div.item to the end
          item.last().after(newItem);
          show_hide_delete_button(items.find('.item'));
          $('[data-toggle="confirmation"]').confirmation({popout: true, btnOkClass: 'btn-success', btnCancelClass: 'btn-danger'});
      });
     
     
  
  // Accepts an element and a function
  function childRecursive(element, func) {
  // Applies that function to the given element.
      func(element);
      var children = element.children();
      if (children.length > 0) {
          children.each(function () {
  // Applies that function to all children recursively
              childRecursive($(this), func);
          });
      }
  }
  
  // Expects format to be xxx-#[-xxxx] (e.g. item-1 or item-1-name)
  function getNewAttr(str, newNum, last) {
      if (last === 'not_set') {
          return str.replace(/\d+/, newNum);
      } else {
          return str.replace(/(\d+)(?!.*\d)/, newNum);
      }
  }
  
  // Written with Twitter Bootstrap form field structure in mind
  // Checks for id, name, and for attributes.
  function setCloneAttr(element, value, last) {
      var last = last || 'not_set';
  // Check to see if the element has an id attribute
      if (element.attr('id') !== undefined) {
  // If so, increment it
          element.attr('id', getNewAttr(element.attr('id'), value, last));
      }
      if (element.attr('name') !== undefined) {
          element.attr('name', getNewAttr(element.attr('name'), value, last));
      }
      if (element.attr('for') !== undefined) {
          element.attr('for', getNewAttr(element.attr('for'), value, last));
      }
  }
  
  // Sets an element's value to ''
  function clearCloneValues(element) {
      if (element.prop("tagName") !== 'OPTION' && element.attr("type") !== 'radio' && element.attr('value') !== undefined) {
          element.val('');
      }
  }
  
  function updateCloneIndex(element, $this) {
      element.each(function (i) {
          childRecursive($(this), function (e) {
              setCloneAttr(e, i + 1);
          });
      });
  }
  
  
  function show_hide_delete_button(item) {
      if (item.length > 1) {
          item.find(".hideable").removeClass("hide");
      } else {
          item.find(".hideable").addClass("hide");
      }
  }
  
  function smooth_remove(element, parent_item) {
      var r = $.Deferred();
      element.slideUp({
          duration: 500,
          complete: function () {
              $(this).remove();
              var item = parent_item.find(".item");
              updateCloneIndex(item, element);
              show_hide_delete_button(item);
          }
      });
      return r;
  }
  
   
   /**************************************************** Confirm Before Delete ****************************************************/
    
  
      $('form').on('click', '.inlineDel', function () {
          var parent_item = $(this).parents('.items'),
                  current_item = $(this).parents('.item'),
                  item = parent_item.find('.item');
          if (item.length > 1) {
              smooth_remove(current_item, parent_item);
          }
  
      });
      
  
      
   /**************************************************** form 12bb show hide ****************************************************/	
    $('#finish').on("click", function(){	
      if(checkValidityElements()) {
        $('#form12bb_form').submit(function(e) {
          e.preventDefault(); 
          if(checkValidityElements())
          {
           $.ajax({
              url: $(this).attr('action'),
              type: 'Post',
              data: $(this).serializeArray(),
              dataType: 'json',
              headers: {
                'X-Requested-With': 'XMLHttpRequest',
              },
              success: function(response) {
                  var responseObject = response;
                  if(responseObject.status == 'success')
                  {
                    $('.pdf-result').show();
                         $('.replace-div').fadeOut(300);
                    $('#iframe').attr('srcdoc',responseObject.data);
                    $('input[name="csrf_test_name"]').val(responseObject.csrf);
                  }
                  else
                  {
                    let errors = responseObject.data;
                    $.each(errors, function(field, message) {
                          var errorElement = $('[name="' + field + '"]').next('.error');
                          errorElement.text(message);
                          errorElement.show(); 
                      });
                  }
              },
              error: function(error) {
                  // Handle the response
                  console.log(error);
              },
          }) 
        }
      });
      } 
  
      })
      $('#show-12bdiv').on("click", function(){		 
          $('.replace-div').fadeIn(300);
          $('.pdf-result').fadeOut(300);
      })
    function getCookie(name) {
    var value = "; " + document.cookie;
    var parts = value.split("; " + name + "=");
    if (parts.length === 2) {
      return parts.pop().split(";").shift();
    }
  }
  function generatepdf(){
  
      var htmlContent = $('#iframe').contents().find('html').html();
      $('input[name="html_content"]').val(htmlContent);
      $('#pdfForm').submit();
  
  }
  function printform(){
    var iframe = document.getElementById('iframe');
    iframe.contentWindow.print();
  }