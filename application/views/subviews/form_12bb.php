<?php
foreach ($values as $key => $value) {
    if(strpos($key, 'deduction_ec') !== false)
    {
        ${$key} = json_decode($value,true);
    }
    else
    {
        ${$key} = $value;
    }
    
}
$roman_numeral_array = ['i','ii','iii','iv','v'];
$no_of_deduction_ec = count($deduction_ec_type);
$deduction_eccd_array = array_combine($deduction_eccd_type, array_map(null,$deduction_eccd,$deduction_eccd_particulars));
$total_deduction_amount=0;
$y=1;
$z=0;
$currentMonth = date('n');
$currentYear = date('Y'); 
if ($currentMonth >= 4) {
    $financialYearStart = $currentYear;
    $financialYearEnd = $currentYear + 1;
} else {
    $financialYearStart = $currentYear - 1;
    $financialYearEnd = $currentYear;
}

$financialYear = $financialYearStart . '-' . $financialYearEnd;
?>
<!DOCTYPE html>
<html>
    <head>
    <style>
        body {
    font-family: 'Open Sans', sans-serif;    
    width: 100%;
    padding: 20px 30px;
    margin: 0;
    font-size: 12px;
    box-sizing: border-box;
}
h1 {

    text-align: center;
    font-weight: 400;
    color: #000;
    /*                margin: 50px auto 20px;*/
    font-size: 24px;
}

h2 {
    display: block;
    text-align: Center;
    font-weight: 400;
    color: #000;
    margin: 0;
    font-size: 18px;
    padding: 20px 0;
}

h2 small {
    display: block;
    font-size: 14px;
    margin-top: 7px;
}


table {
    margin: 0 0 30px;
    font-size: 12px;
    font-family: 'Open Sans', sans-serif;
}
td {
    word-wrap: break-word;
    border: 1px solid #000;
    vertical-align: top;
}

.header {
    width: 100%;
    position: fixed;
}

.footer {
    width: 100%;
    text-align: center;
    position: fixed;
}
.header {
    top: 0px;
}

.footer {
    bottom: 0px;
}

.footercreated, .footerbrand {
    width: 100%;
    position: fixed;
    text-align: right;
    bottom: 0px;
}

.footerbrand {
    text-align: left;
}
.pagenum:before {
    content: counter(page);
}

.flat-table {
    width: 100%;
    border-collapse: collapse;

}

.flat-table td:last-child, .flat-table th:last-child { border-right: 1px solid #000}
.flat-table tr:last-child td{ border-bottom: 1px solid #000}

.flat-table .th {
    width: 25%;
    color: #000;
    font-weight: bold;
    padding: 8px 15px;
    text-align: center;
    border-top: 1px solid #000;
    border-left: 1px solid #000;
    box-sizing: border-box;
}

.flat-table .thsub {
    color: #000;
    font-weight: bold;
    padding: 8px 15px;
    text-align: left;
    border-top: 1px solid #000;
    border-top: 1px solid #000;
    border-left: 1px solid #000;
    box-sizing: border-box;

}

.flat-table td {
    color: #061D3E;
    padding: 8px 15px;
    border-top: 1px solid #000;
    border-left: 1px solid #000;
    text-align: left;
    box-sizing: border-box;
    /* -webkit-print-color-adjust: exact; */
}


.flat-table .tdm {
    color: #061D3E;
    padding: 8px 35px;
    border-top: 1px solid #000;
    border-left: 1px solid #000;
    text-align: left;
    box-sizing: border-box;
    /* -webkit-print-color-adjust: exact; */
}

.c2, .c3, .text-right {
    text-align: right !important;
}
.text-left {
    text-align: left !important;
}

.page-break-before {
    page-break-inside:avoid; 
    page-break-after:auto;
}
.botless
{
    border-bottom: none !important;
}
.topless
{
    border-top: none !important;
}

    </style>       
    </head>
    
    <body>
    <div class="footer">
    Page <span class="pagenum"></span>
    </div>
    <div class="footerbrand">Form 12BB generated using Tax2win.in</div>
    <div class="footercreated">2023-05-22 16:34:34</div>
    <img height="60" src="data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIGlkPSJMYXllcl8xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4PSIwcHgiIHk9IjBweCIKCSB3aWR0aD0iMTAwMHB4IiBoZWlnaHQ9IjUwMHB4IiB2aWV3Qm94PSIwIDAgMTAwMCA1MDAiIGVuYWJsZS1iYWNrZ3JvdW5kPSJuZXcgMCAwIDEwMDAgNTAwIiB4bWw6c3BhY2U9InByZXNlcnZlIj4KPHBhdGggZmlsbC1ydWxlPSJldmVub2RkIiBjbGlwLXJ1bGU9ImV2ZW5vZGQiIGZpbGw9IiMwQjIzNDMiIGQ9Ik0zOTYuMzczLDIxNy43NHY5MC45MWgtMjcuNjcydi05MC45MWgtMzIuMzQ4di0xMC43OTQKCWMwLTMuMjgsMS4yNTQtNi41NjEsMy43NTctOS4wNjljMi41MDMtMi41MDMsNS43OTQtMy43NTcsOS4wNzUtMy43NTdoNzkuNTM3djEwLjc4OGMwLDMuMjg2LTEuMjQ5LDYuNTY2LTMuNzUxLDkuMDc1CglzLTUuNzk0LDMuNzU3LTkuMDc1LDMuNzU3SDM5Ni4zNzN6Ii8+CjxwYXRoIGZpbGwtcnVsZT0iZXZlbm9kZCIgY2xpcC1ydWxlPSJldmVub2RkIiBmaWxsPSIjMEIyMzQzIiBkPSJNNDcxLjkxNSwyNzMuNTU1aC0zLjM5NGMtMTEuNjUxLDAtMjQuNzYsMS40NTMtMjQuNzYsMTAuOTkzCgljMCw2LjE1MSw2LjE1Miw4LjI1NywxMS40ODYsOC4yNTdjMTAuNjgxLDAsMTYuNjY4LTYuNDgsMTYuNjY4LTE2LjQ5N1YyNzMuNTU1TDQ3MS45MTUsMjczLjU1NXogTTQyMy4yMTcsMjM5LjA5CgljOS4zODYtOC44OTksMjIuODE0LTEzLjU4NiwzNS45MTctMTMuNTg2YzI3LjAyLDAsMzcuMDQyLDEzLjI2OSwzNy4wNDIsNDIuNTQxdjQwLjYwNWgtMTYuNTcyYy0xLjk2MywwLTMuOTM4LTAuNzQzLTUuNDM3LTIuMjQ3CgljLTEuNTA0LTEuNTA0LTIuMjUzLTMuNDc5LTIuMjUzLTUuNDQ4di0wLjg3NGgtMC40ODhjLTQuMDQ2LDYuNjM1LTEzLjI2OSwxMC41MTEtMjIuODA4LDEwLjUxMWMtMTIuNzgxLDAtMjkuMjg0LTYuMy0yOS4yODQtMjUuMjI3CgljMC0yMy4yOTYsMjguMzE5LTI3LjE4NCw1MS43NjktMjcuMTg0di0xLjI4OGMwLTcuOTM0LTYuMy0xMS42NTEtMTQuNTYzLTExLjY1MWMtNy41OTksMC0xNS4wMzksMy43MTctMTkuODk3LDguMjQ2TDQyMy4yMTcsMjM5LjA5Cgl6Ii8+CjxwYXRoIGZpbGwtcnVsZT0iZXZlbm9kZCIgY2xpcC1ydWxlPSJldmVub2RkIiBmaWxsPSIjMEIyMzQzIiBkPSJNNTc3LjU4NiwzMDguNjQ5Yy0yLjAwNCwwLTQuMDIzLTAuNDU5LTUuOTE5LTEuNDQxCgljLTEuOTA3LTAuOTkzLTMuNDU2LTIuMzgzLTQuNTk4LTQuMDIzTDU1My41OTcsMjgzLjlsLTEzLjk2MSwxOS40MjFjLTEuMTQxLDEuNTk1LTIuNjg5LDIuOTY4LTQuNTU3LDMuOTI3CgljLTEuODc5LDAuOTU5LTMuODgyLDEuNDAxLTUuODU2LDEuNDAxaC0yNC4xNTlsMzIuMTg5LTQyLjIxN2wtMjkuMTA4LTM4LjM0MWgyNC4yMTZjMS45OTgsMCw0LjAxOSwwLjQ2NSw1LjkxNCwxLjQ1MwoJYzEuOTA2LDAuOTg4LDMuNDUsMi4zODQsNC41OTcsNC4wMjRsMTEuMjE0LDE2LjAzOGwxMS4zNzktMTYuMDg5YzEuMTQ2LTEuNjIzLDIuNjczLTMuMDA4LDQuNTY4LTMuOTg0CgljMS44OS0wLjk3NiwzLjkxLTEuNDQxLDUuOTA4LTEuNDQxaDIzLjI3M2wtMjguOTU1LDM4LjM0MWwzMS44NzIsNDIuMjE3SDU3Ny41ODZ6Ii8+CjxwYXRoIGZpbGwtcnVsZT0iZXZlbm9kZCIgY2xpcC1ydWxlPSJldmVub2RkIiBmaWxsPSIjNUJCQTQ5IiBkPSJNODE5LjA1NywzMDguNjQ5SDc5Mi4zNWwtMTUuODQ1LTUwLjE1aC0wLjMyM2wtMTUuNTMzLDUwLjE1CgloLTI2LjY5bC0yNy44MjUtODAuNTU4aDE4Ljg0MmMyLjcxOSwwLDUuNDQ4LDAuODUxLDcuNzg2LDIuNjMzYzIuMzE1LDEuNzc2LDMuODcsNC4yLDQuNTgsNi44MTZsMTEuMzMzLDQxLjUwN2gwLjQ5NGwxNC4zOTItNTAuOTU3CgloMjYuNzAybDE0Ljg2OCw1MC45NTdoMC4zMjlsMTEuNDc2LTQxLjU0MmMwLjcyMS0yLjYxNiwyLjI2NS01LjAyMiw0LjU5MS02Ljc5M2MyLjMyNy0xLjc3MSw1LjA1Ny0yLjYyMiw3Ljc3LTIuNjIyaDE3LjczNAoJTDgxOS4wNTcsMzA4LjY0OXoiLz4KPHBhdGggZmlsbC1ydWxlPSJldmVub2RkIiBjbGlwLXJ1bGU9ImV2ZW5vZGQiIGZpbGw9IiM1QkJBNDkiIGQ9Ik04NzAuOTU2LDIxNi45MjhjLTguNTY5LDAtMTUuMjEtNi42MzQtMTUuMjEtMTQuNTU3CgljMC03Ljc2NCw2LjY0MS0xNC41NjMsMTUuMjEtMTQuNTYzYzguNDE2LDAsMTUuMjA5LDYuNDcsMTUuMjA5LDE0LjU2M0M4ODYuMTY1LDIxMC42MjMsODc5LjM3MiwyMTYuOTI4LDg3MC45NTYsMjE2LjkyOHoiLz4KPHBhdGggZmlsbC1ydWxlPSJldmVub2RkIiBjbGlwLXJ1bGU9ImV2ZW5vZGQiIGZpbGw9IiM1QkJBNDkiIGQ9Ik04NTcuNjkzLDMwOC42NDl2LTY3LjcyN2MwLTMuMjg2LDEuMjQ4LTYuNTY2LDMuNzU3LTkuMDc1CglzNS43ODgtMy43NTcsOS4wNzQtMy43NTdoMTMuN3Y4MC41NThIODU3LjY5M3oiLz4KPHBhdGggZmlsbC1ydWxlPSJldmVub2RkIiBjbGlwLXJ1bGU9ImV2ZW5vZGQiIGZpbGw9IiM1QkJBNDkiIGQ9Ik05NTguMTU0LDMwOC42NDlWMjY0LjY1YzAtOC44OTgtMi40MjMtMTYuOTgtMTEuOTgtMTYuOTgKCWMtOS4zODEsMC0xNC4yMzMsOC4wODItMTQuMjMzLDE3LjMwM3Y0My42NzZoLTI2LjY4NHYtODAuNTU4aDEyLjg4MmMzLjI4LDAsNi41NjYsMS4yNDksOS4wNzUsMy43NTcKCWMyLjQ5NywyLjUwOCwzLjc1Nyw1Ljc4OSwzLjc1Nyw5LjA3NXYtMS42NjhoMC4zMjNjMy43MjMtNy4xMjMsMTIuOTQ1LTEzLjQyMiwyNC4wOTctMTMuNDIyYzIxLjY5LDAsMjkuNDQ4LDE2LjgyMSwyOS40NDgsMzIuOTk2Cgl2NDkuODIxSDk1OC4xNTR6Ii8+CjxwYXRoIGZpbGwtcnVsZT0iZXZlbm9kZCIgY2xpcC1ydWxlPSJldmVub2RkIiBmaWxsPSIjMEIyMzQzIiBkPSJNNjkxLjcwMSwyODguNzkzYy0yLjUwMy0yLjUwOS01Ljc4OC0zLjc1OC05LjA2OC0zLjc1OGgtMzMuOTIxCglsMjYuNTI1LTIzLjYxNGMxMC41MTctOS4zODEsMjAuMDYyLTE4LjkzMiwyMC4wNjItMzUuMjcxYzAtMjMuOTQzLTIwLjM4NS0zNS4xMDYtNDEuNDE3LTM1LjEwNgoJYy0yMi4zMjUsMC00MS4wODIsMTMuNzU3LTQzLjY3LDM2Ljg4OWwtMC43NjYsMTEuMjU5aDEzLjk5NGMzLjIwNywwLDcuNDQ2LTEuNjkxLDguODM3LTMuNjI2CgljNC4zMTMtNS45NTMsNS41NDQtMjEuMzg0LDIwLjE1Mi0yMS4zODRjOC41NzUsMCwxNC4yMzgsNS42NTgsMTQuMjM4LDEzLjc1MWMwLDYuOTUyLTMuNzIzLDEyLjI5Mi05LjM4MSwxNy42MzhsLTQ0LjY1MSw0MC40MzUKCXYyMi42NDRoODIuODI4di0xMC43NzZDNjk1LjQ2NCwyOTQuNTgxLDY5NC4yMDQsMjkxLjMwNyw2OTEuNzAxLDI4OC43OTN6Ii8+CjxwYXRoIGZpbGwtcnVsZT0iZXZlbm9kZCIgY2xpcC1ydWxlPSJldmVub2RkIiBmaWxsPSIjMEIyMzQzIiBkPSJNMjQ0LjY0MywyNjEuMmMtNS4yOSw0My4wMzQtNDIuMDUzLDc2LjQ3My04Ni40ODksNzYuNDczCgljLTI0LjI1NSwwLTQ2LjIxOC05Ljk2Ni02Mi4wMjktMjUuOTk4bC0xNC4xMDgsMTQuMTE0bC0xLjU2NiwxLjU2NmMxOS44MzQsMjAuMDQ0LDQ3LjMzNiwzMi40ODksNzcuNzA0LDMyLjQ4OQoJYzYwLjI5MywwLDEwOS4zNDktNDkuMDU2LDEwOS4zNDktMTA5LjM0OGMwLTMuODkzLTAuMjE2LTcuNzQxLTAuNjEzLTExLjUzOEwyNDQuNjQzLDI2MS4yeiIvPgo8cGF0aCBmaWxsLXJ1bGU9ImV2ZW5vZGQiIGNsaXAtcnVsZT0iZXZlbm9kZCIgZmlsbD0iIzBCMjM0MyIgZD0iTTcwLjk3OSwyNTAuNDk3YzAtNDguMDYzLDM5LjEwNy04Ny4xNyw4Ny4xNzUtODcuMTcKCWMzMi4xOTUsMCw2MC4zNTUsMTcuNTU5LDc1LjQ0NSw0My41OTZsMTYuMTEyLTE2LjEwNmMtMTkuNTQtMjkuODY4LTUzLjI3Mi00OS42NjMtOTEuNTU3LTQ5LjY2MwoJYy01Ni4zNDIsMC0xMDIuODU2LDQyLjgyNS0xMDguNzEyLDk3LjYyOWwyMi4xODksMjIuMTg5QzcxLjIxNywyNTcuNTM5LDcwLjk3OSwyNTQuMDUsNzAuOTc5LDI1MC40OTd6Ii8+CjxwYXRoIGZpbGwtcnVsZT0iZXZlbm9kZCIgY2xpcC1ydWxlPSJldmVub2RkIiBmaWxsPSIjNTZCOTRCIiBkPSJNNzYuNTY4LDMyMC4zNDFsOTcuMzQtOTcuMzM0bC0xMi44ODMtMTIuODk0CgljLTIuMjctMi4yNy01LjQxNC0zLjY3Ny04Ljg4Ny0zLjY3N2MtMy40NjcsMC02LjYwNSwxLjQwNy04Ljg4MSwzLjY3N2wtNjYuNjg4LDY2LjY4OEw0Ny4xNiwyNDcuMzkzCgljLTIuMTA1LTIuMTA1LTUuMDE3LTMuNDExLTguMjI5LTMuNDExYy0zLjIxMiwwLTYuMTI5LDEuMzA2LTguMjI5LDMuNDA1bC0xMy41NDEsMTMuNTQxTDc2LjU2OCwzMjAuMzQxeiIvPgo8cGF0aCBmaWxsLXJ1bGU9ImV2ZW5vZGQiIGNsaXAtcnVsZT0iZXZlbm9kZCIgZmlsbD0iIzVCQkE0OSIgZD0iTTE1NC4xNDcsMjU2LjMzMWwyMC40NjQsMjAuNDdsOTIuNDcxLTkyLjQ2NQoJYzIuMjctMi4yNyw1LjQwOC0zLjY3Nyw4Ljg3Ni0zLjY3N2MzLjQ3MywwLDYuNjExLDEuNDA3LDguODg3LDMuNjc3bDEyLjg4MiwxMi44ODhMMTc0LjYxMiwzMjAuMzQxbC00Mi43MjItNDIuNzIzTDE1NC4xNDcsMjU2LjMzMQoJeiIvPgo8L3N2Zz4=">
    <p style="font-family: 'Open Sans', sans-serif; font-size: 20px; margin-bottom: 0px; text-align: center;">
    <strong>FORM NO.12BB</strong>
    </p>
    
    <p style="text-align:center; font-family: 'Open Sans', sans-serif; margin-top: 0px">(Rule 26C)</p>
    <h2 class="th" colspan="2">Statement showing particulars of claims by an employee for deduction of tax under section 192</h2>
    <table class="flat-table" id="headingtab" border="0">
    <tbody>
    <tr>
    <td class="thsub" style="width: 50%">Name and address of the employee:</td>
    <td style="text-align: left; width:50%">
    <?php echo $name ?><br> <?php echo $address ?><br>
    <?php echo $emailid ?>
    </td>
    </tr>
    <tr>
    <td class="thsub" style=" width:50%">Permanent Account Number of the employee:</td>
    <td style="text-align: left; width:50%">
    <?php echo $pan ?>
    </td>
    </tr>
    <tr>
    <td class="thsub" style=" width:50%">Financial Year:</td>
    <td style="text-align: left; width:50%">
    <?php echo $financialYear ?> </td>
    </tr>
    </tbody>
    </table>
    <div class="page-break-before"></div>
    <p style="text-align:center; font-size: 16px;">
    <strong>Details of claims and evidence thereof</strong>
    </p>
    <table class="flat-table">
    <tbody><tr>
    <td style="width:1%" class="th text-left">S.No.</td>
    <td style="width:59%" class="th text-left">Nature of claim</td>
    <td style="width:20%" class="th text-left">Amount(Rs.)</td>
    <td style="width:20%" class="th text-left">Evidence / Particulars</td>
    </tr>
    <tr>
    <td class="thsub botless" style="width:1%">1</td>
    <td class="thsub botless" style="width:59%">House Rent Allowance:</td>
    <td class="thsub botless" style="width:20%"></td>
    <td class="thsub botless" style="width:20%"></td>
    </tr>
    <tr>
    <td class="botless topless" style="width:1%"></td>
    <td class="botless topless" style="padding-left:28px; width:59%; word-wrap:">(i) Rent paid to the landlord</td>
    <td class="botless topless currency_field" style="width:20%"><?php echo $hra_rent ?>
    </td>
    <td class="botless topless" style="width:20%; border:1px solid #000"><?php echo $hra_particulars ?>
    </td>
    </tr>
    <tr>
    <td class="botless topless" style="width:1%"></td>
    <td class="botless topless" style="padding-left:28px; width:59%; word-wrap:">(ii) Name of the landlord</td>
    <td class="botless topless" style="width:20%"></td>
    <td class="botless topless" style="width:20%"><?php echo $landlord_name ?>
    </td>
    </tr>
    <tr>
    <td class="botless topless" style="width:1%"></td>
    <td class="botless topless" style="padding-left:28px;width:59%; word-wrap:">(iii) Address of the landlord</td>
    <td class="botless topless" style="width:20%"></td>
    <td class="botless topless" style="width:20%"><?php echo $landlord_address ?>
    </td>
    </tr>
    <tr>
    <td class="botless topless" style="width:1%"></td>
    <td class="botless topless" style="padding-left:28px;width:59%; word-wrap:">(iv) Permanent Account Number of the landlord</td>
    <td class="botless topless" style="width:20%"></td>
    <td class="botless topless" style="width:20%"><?php echo $landlord_pan ?>
    </td>
    </tr>
    <tr>
    <td class="topless" style="width:1%"></td>
    <td colspan="3" style="width:30%; word-wrap:">
    Note: Permanent Account Number shall be furnished if the aggregate rent paid during the previous year exceeds one lakh rupees
    </td>
    </tr>
    <tr>
    <td class="thsub" style="width:1%">2</td>
    <td class="thsub" style="width:59%">Leave travel concessions or assistance</td>
    <td class="currency_field" style="width:20%"><?php echo $travel_expenses ?></td>
    <td style="width:20%; border:1px solid #000"><?php echo $travel_particulars ?></td>
    </tr>
    <tr>
    <td class="botless thsub" style="width:1%">3</td>
    <td class="thsub botless" style="width:59%">Deduction of interest on borrowing:</td>
    <td class="botless" style="width:20%"></td>
    <td class="botless" style="width:20%">></td>
    </tr>
    <tr>
    <td class="botless topless" style="width:1%"></td>
    <td class="botless topless" style="padding-left:28px; width:59%">(i) Interest payable/paid to the lender</td>
    <td class="botless topless currency_field" style=" width:20%"><?php echo $home_loan_interest ?></td>
    <td class="botless topless" style=" width:20%"><?php echo $home_loan_particulars ?></td>
    </tr>
    <tr>
    <td class="botless topless" style="width:1%"></td>
    <td class="botless topless" style="padding-left:28px; width:59%">(ii) Name of the lender</td>
    <td class="botless topless" style=" width:20%"></td>
    <td class="botless topless" style=" width:20%"><?php echo $loan_provider ?></td>
    </tr>
    <tr>
    <td class="botless topless" style="width:1%"></td>
     <td class="botless topless" style="padding-left:28px; width:59%">(iii) Address of the lender</td>
    <td class="botless topless" style=" width:20%"></td>
    <td class="botless topless" style=" width:20%"><?php echo $lender_address ?></td>
    </tr>
    <tr>
    <td class="botless topless" style="width:1%"></td>
    <td class="botless topless" style="padding-left:28px; width:59%">(iv) Permanent Account Number of the lender</td>
    <td class="botless topless" style=" width:20%"></td>
    <td class="botless topless" style=" width:20%"><?php echo $lender_pan ?></td>
    </tr>
    <tr>
    <td class="botless topless" style="width:1%"></td>
    <td class="botless topless" style="padding-left:28px; width:59%">(a) Financial Institutions(if available)</td>
    <td class="botless topless" style=" width:20%"></td>
    <td class="botless topless" style=" width:20%"></td>
    </tr>
    <tr>
    <td class="botless topless" style="width:1%"></td>
    <td class="botless topless" style="padding-left:28px; width:59%">(b) Employer(if available)</td>
    <td class="botless topless" style=" width:20%"></td>
    <td class="botless topless" style=" width:20%"></td>
    </tr>
    <tr>
    <td class="topless" style="width:1%"></td>
    <td class="topless" style="padding-left:28px; width:59%">(c) Others</td>
    <td class="topless" style=" width:20%"></td>
    <td class="topless" style=" width:20%"></td>
    </tr>
    <tr>
    <td class="botless thsub" style="width:1%">4</td>
    <td class="botless thsub" style="width:59%">Deduction under Chapter VI-A</td>
    <td class="botless thsub" style="width:20%"></td>
    <td class="botless" style="width:20%"></td>
    </tr>
    <tr>
    <td class="botless topless thsub" style="width:1%"></td>
    <td class="botless topless thsub" style="width:59%">(A). Section 80C,80CCC and 80CCD</td>
    <td class="botless topless thsub" style="width:20%"></td>
    <td class="botless topless" style="width:20%"></td>
    </tr>
    <tr>
    <td class="botless topless" style="width:1%"></td>
    <td class="botless topless" style="padding-left:28px; width:59%">(i) Section 80C</td>
    <td class="botless topless" style=" width:20%"></td>
    <td class="botless topless" style=" width:20%"></td>
    </tr>
    <?php
    if($no_of_deduction_ec >1) {
         for ($i = ord('a'); $i < ord('a') + $no_of_deduction_ec ; $i++) { ?>
        <tr>
        <td class="botless topless" style="width:1%"></td>
        <td class="botless topless" style="padding-left:56px; width:59%"><?php echo '('.chr($i).')'.' '.$deduction_ec_type[$y] ?></td>
        <td class="botless topless" style=" width:20%"><?php echo $deduction_ec[$y] ?></td>
        <td class="botless topless" style=" width:20%"><?php echo $deduction_ec_particulars[$y] ?></td>
        <?php
        $total_deduction_amount += $deduction_ec[$y];
         $y+=1;
        ?>    
        </tr>
    <?php }
        } ?>
    <?php if($no_of_deduction_ec >1) { ?>
    <tr>
    <td class="botless topless" style="width:1%"></td>
    <td class="botless topless" style="padding-left:56px; width:59%;"><?php echo 'Total - Section80C (a to'.' '.chr(ord('a') + $no_of_deduction_ec - 1).')' ?></td>
    <td class="botless topless" style=" width:20%; border: 1px solid black !important;"><?php echo $total_deduction_amount ?></td>
    <td class="botless topless" style=" width:20%"></td>
    </tr>
    <?php } ?>
    <tr>
    <td class="botless topless" style="width:1%"></td>
    <td class="botless topless" style="padding-left:28px; width:59%">(ii) Section 80CCC</td>
    <td class="botless topless currency_field" style=" width:20%"><?php echo array_key_exists('Sec 80 CCC - Deduction for contribution to Certain Pension Funds',$deduction_eccd_array) ? $deduction_eccd_array['Sec 80 CCC - Deduction for contribution to Certain Pension Funds'][0] : ''; ?></td>
    <td class="botless topless" style=" width:20%"><?php echo array_key_exists('Sec 80 CCC - Deduction for contribution to Certain Pension Funds',$deduction_eccd_array) ? $deduction_eccd_array['Sec 80 CCC - Deduction for contribution to Certain Pension Funds'][1] : ''; unset($deduction_eccd_array['Sec 80 CCC - Deduction for contribution to Certain Pension Funds']); ?></td>
    </tr>
    <tr>
    <td class="botless topless" style="width:1%"></td>
    <td class="botless topless" style="padding-left:28px; width:59%">(iii) Section 80CCD</td>
    <td class="botless topless currency_field" style=" width:20%"><?php echo array_key_exists('Sec 80 CCD - Contribution to NPS',$deduction_eccd_array) ? $deduction_eccd_array['Sec 80 CCD - Contribution to NPS'][0] : ''; ?></td>
    <td class="botless topless" style=" width:20%"><?php echo array_key_exists('Sec 80 CCD - Contribution to NPS',$deduction_eccd_array) ? $deduction_eccd_array['Sec 80 CCD - Contribution to NPS'][1] : ''; unset($deduction_eccd_array['Sec 80 CCD - Contribution to NPS']); ?></td>
    </tr>
    <tr>
    <td class="topless botless" style="width:1%"></td>
    <td class="topless botless thsub" style="width:59%">
    (B) Other sections (e.g. 80E, 80G, 80TTA, etc.) under Chapter VI-A.</td>
    <td class="topless botless" style="width:20%"></td>
    <td class="topless botless" style="width:20%"></td>
    </tr>
    <?php if (count($deduction_eccd_array)>0) {
         foreach($deduction_eccd_array as $key=>$value) { 
                 ?>
                <tr>
                <td class="botless topless" style="width:1%"></td>
                <td class="botless topless" style="padding-left:28px; width:59%"><?php echo '('.$roman_numeral_array[$z].') '.$key ?> </td>
                <td class="botless topless currency_field" style=" width:20%"><?php echo $value[0] ?></td>
                <td class="botless topless" style=" width:20%"><?php echo $value[1] ?></</td>
                </tr>
        <?php $z+=1; } } ?>
    <tr>
    <td class="thsub" style="text-align: center;" colspan="4">Verification</td>
    </tr>
    <tr>
    <td class="" style="" colspan="4">
    I,
    <strong><?php echo $name ?></strong>, son/daughter of
    <strong><?php echo $father_name ?></strong> do hearby certify that the information given above is complete
    and correct.
    </td>
    </tr>
    <tr>
    <td class="" style="width: 40%" colspan="2">Place: <strong><?php echo $place ?></strong></td>
    <td class="botless" style="width: 40%" colspan="2"></td>
    </tr>
    <tr>
    <td class="" style="width: 40%" colspan="2">Date: <strong><?php echo date('d-m-y'); ?></strong></td>
    <td class="topless" style="vertical-align: bottom;width: 60%" colspan="2">Signature of the employee</td>
    </tr>
    <tr>
    <td class="" style="width: 40%" colspan="2">Designation:</td>
    <td class="" style="width: 60%" colspan="2">Full Name: <strong><?php echo $name ?></strong></td>
    </tr>
    </tbody>
    </table>
    </body>
    </html>