<header class="clearfix inr-new-header">
  <section class="bottom">
    <div class="container">
      <div class="row">
        <div class="col-md-2 col-sm-3 col-xs-12 logo"> <a href="index.html" class="logo"><img src="application/views/assets/img/logo.png"  title="Income Tax Filing Online" alt="Income Tax Filing Online" /> </a> </div>
        <div class="col-md-10 col-sm-9 col-xs-12"> 
          <!-- Static navbar -->
          <nav class="navbar">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
              <ul class="nav navbar-nav">
                <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Services <span class="caret"></span></a>
                  <ul class="dropdown-menu">
                    <li><a href="https://tax2win.in/efile-income-tax-return/upload-form16" title="File ITR from Form 16">Upload Form 16</a></li>
                    <li><a href="https://tax2win.in/efile-income-tax-return/sources-of-income" title="File Income Tax Return">File Your Return</a></li>
                    <li><a href="https://tax2win.in/ca-assisted" title="File ITR with CA">CA Assisted Tax Filing</a></li>
                    <li><a href="https://tax2win.in/income-tax-refund-status" title="Income Tax Refund Status">Refund Status</a></li>
                    <li><a href="https://tax2win.in/online-pan-card" title="PAN Card Online">Apply PAN Card Online</a></li>
                  </ul>
                </li>
                <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Tools <span class="caret"></span></a>
                  <ul class="dropdown-menu">
                    <li><a href="https://tax2win.in/tax-tools/income-tax-calculator" title="Income Tax Calculator">Income Tax Calculator</a></li>
                    <li><a href="https://tax2win.in/tax-tools/hra-calculator" title="HRA Calculator">HRA Calculator</a></li>
                    <li><a href="https://tax2win.in/tax-tools/gratuity-calculator" title="Gratuity Calculator">Gratuity Calculator</a></li>
                    <li><a href="https://tax2win.in/tax-tools/rent-receipt" title="Rent Receipt Generator">Rent Receipt Generator</a></li>
                    <li><a href="https://tax2win.in/budget-2017-impact" title="Budget Impact Calculator 2017">Budget Impact Calculator 2017</a></li>
                  </ul>
                </li>
                <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Knowledge Center <span class="caret"></span></a>
                  <ul class="dropdown-menu">
                    <li><a href="#">Blog</a></li>
                    <li><a href="#">Guides</a></li>
                    <li><a href="#">Faq</a></li>
                    <li><a href="#">News</a></li>
                  </ul>
                </li>
                <li><a href="#">About</a></li>
                <li><a href="#">Contact Us</a></li>
                <li class="log-in"><a href="#">Login</a>/ <a href="#">Signup</a> </li>
              </ul>
            </div>
          </nav>
        </div>
      </div>
    </div>
  </section>
</header>