<footer class="footer-half">
    <section class="contact-info">
      <div class="container">
        <div class="col-md-7 col-sm-9 col-xs-12 col-md-offset-1"> <img src="application/views/assets/img/calling.png" alt="" class="img-responsive" width="40"> <span>+91 9660-99-66-55<small>&nbsp;|&nbsp;</small><a href="mailto:support@tax2win.in">support@tax2win.in</a></span> </div>
        <div class="col-md-4 col-sm-3 col-xs-12">
          <div class="social-icons"> <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a> <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a> <a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a> <a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a> <a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a> </div>
        </div>
      </div>
    </section>
    <section class="copyright">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12">© 2016 TAX2WIN. All Rights Reserved.</div>
          <div class="col-md-6 col-sm-6 col-xs-12 text-right"><a href="#">Terms & Conditions </a> | <a href="#">Privacy Policy</a></div>
        </div>
      </div>
    </section>
  </footer>