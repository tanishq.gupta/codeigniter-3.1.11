<div class="content-panel">
    <div class="container" id="form_container">
      <div class="row replace-div">
        <div class="col-md-12">
          <ul class="nav nav-tabs">
            <li class="active"><a href="#tab1" data-toggle="tab"><img src="application/views/assets/img/form-12bb/12bb-1.png" alt="Details of Employee" title="Details of Employee"><span>Details of <br>
              Employee</span></a></li>
            <li><a href="#tab2" data-toggle="tab"> <img src="application/views/assets/img/form-12bb/12bb-2.png" alt="House Rent Allowance" title="House Rent Allowance"> <span>House Rent <br>
              Allowance</span></a></li>
            <li><a href="#tab3" data-toggle="tab"><img src="application/views/assets/img/form-12bb/12bb-3.png" alt="Leave Travel Concessions" title="Leave Travel Concessions"> <span>Leave Travel<br>
              Concessions</span></a></li>
            <li><a href="#tab4" data-toggle="tab"><img src="application/views/assets/img/form-12bb/12bb-4.png" alt="Interest on Home Loan" title="Interest on Home Loan"> <span>Interest on<br>
              Home Loan</span></a></li>
            <li><a href="#tab5" data-toggle="tab"><img src="application/views/assets/img/form-12bb/12bb-5.png" alt="Deduction under Chapter VI - A" title="Deduction under Chapter VI - A"> <span>Deduction under <br>
              Chapter VI - A</span></a></li>
          </ul>
          <?php echo form_open(base_url('form12bb/processdata'),array('method'=>'post','id'=>'form12bb_form')) ?>
            <div class="tab-content clearfix">
              <div class="tab-pane active smooth-load" id="tab1">
                <div class="row">
                  <div class="col-md-12 text-center">
                    <div class="badge">1</div>
                    <h2>Details of Employee</h2>
                    <div class="hr"></div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-4 col-sm-6 col-xs-12 form-group">
                    <label for="name">Name <em>*</em></label>
                    <input type="text" placeholder="Name" id="name" name="name" maxlength="100" class="form-control" required >
                    <span class="error"></span>
                  </div>
                  <div class="col-md-4 col-sm-6 col-xs-12  form-group">
                    <label for="pan">PAN <em>*</em></label>
                    <input type="text" placeholder="PAN No." name="pan" id="pan" maxlength="10" class="form-control pan" pattern = "[A-Z]{5}[0-9]{4}[A-Z]{1}" required >
                    <span class="error"></span>
                  </div>
                  <div class="col-md-4 col-sm-6 col-xs-12  form-group">
                    <label for="father_name">Father's Name <em>*</em></label>
                    <input type="text" placeholder="Father's Name" name="father_name" id="father_name" maxlength="100" class="form-control" required >
                    <span class="error"></span>
                  </div>
                  <div class="col-md-4 col-sm-6 col-xs-12  form-group">
                    <label for="place">Place<em>*</em></label>
                    <input type="text" placeholder="Place" name="place" id="place" class="form-control" maxlength="100"required >
                    <span class="error"></span>
                  </div>
                  <div class="col-md-4 col-sm-6 col-xs-12  form-group">
                    <label for="emailid">Email Id <em>*</em></label>
                    <input type="email" placeholder="Email" name="emailid" id="emailid" class="form-control" required >
                    <span class="error"></span>
                  </div>
                  <div class="col-md-4 col-sm-6 col-xs-12  form-group">
                    <label for="mobile">Mobile <em>*</em></label>
                    <input type="text" placeholder="Mobile" name="mobile" id="mobile" class="form-control amount" maxlength="10" required >
                    <span class="error"></span>
                  </div>
                  <div class="col-md-12 col-sm-12 col-xs-12  form-group">
                    <label for="address">Address <em>*</em></label>
                    <textarea class="form-control" rows="2" id="address" name="address" required ></textarea>
                    <span class="error"></span>

                  </div>
                  <div class="col-md-12 col-sm-12 col-xs-12  text-center"> <a class="green-btn btnNext" >Next</a> </div>
                </div>
              </div>
              <div class="tab-pane smooth-load" id="tab2">
                <div class="row">
                  <div class="col-md-12 col-sm-12 col-xs-12  text-center">
                    <div class="badge">2</div>
                    <h2>House Rent Allowance</h2>
                    <div class="hr"></div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-4 col-sm-6 col-xs-12  form-group">
                    <label for="name">Rent paid to Landlord</label>
                    <input type="text" placeholder="Rent paid to Landlord" name="hra_rent" class="form-control amount">
                    <span class="error"></span>
                  </div>
                  <div class="col-md-4 col-sm-6 col-xs-12  form-group">
                    <label for="name">Name of Landlord</label>
                    <input type="text" placeholder="Name of Landlord" name="landlord_name" class="form-control">
                    <span class="error"></span>
                  </div>
                  <div class="col-md-4 col-sm-6 col-xs-12  form-group">
                    <label for="name">Evidence Particlars</label>
                    <input type="text" placeholder="Evidence Particlars" name="hra_particulars" class="form-control">
                    <span class="error"></span>
                  </div>
                  <div class="col-md-8 col-sm-6 col-xs-12  form-group">
                    <label for="name">Address of Landlord</label>
                    <input type="text" placeholder="Address of Landlord" name="landlord_address" class="form-control">
                    <span class="error"></span>
                  </div>
                  <div class="col-md-4 col-sm-6 col-xs-12  form-group">
                    <label for="name">PAN of Landlord </label>
                    <input type="text" placeholder="PAN of Landlord" name="landlord_pan" class="form-control pan" pattern="[A-Z]{5}[0-9]{4}[A-Z]{1}">
                    <span class="error"></span>
                  </div>
                  <div class="col-md-12 col-sm-12 col-xs-12 ">
                    <p> If the aggregate rent paid during the previous year exceeds one lakh rupees :</p>
                  </div>
                  <div class="col-md-12 col-sm-12 col-xs-12  text-center"> <br>
                    <a class="gray-btn btnPrevious" >Back</a> &nbsp; <a class="green-btn btnNext" >Next</a> </div>
                </div>
              </div>
              <div class="tab-pane smooth-load" id="tab3">
                <div class="row">
                  <div class="col-md-12 col-sm-12 col-xs-12  text-center">
                    <div class="badge">3</div>
                    <h2>Leave Travel Concessions </h2>
                    <div class="hr"></div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6 col-sm-6 col-xs-12  form-group">
                    <label for="name"> Amount (Your total expenditure on travel )</label>
                    <input type="text" placeholder="Amount" name="travel_expenses" class="form-control amount">
                    <span class="error"></span>
                  </div>
                  <div class="col-md-6 col-sm-6 col-xs-12  form-group">
                    <label for="name">Evidence of expenditure</label>
                    <input type="text" placeholder="Evidence of expenditure" name="travel_particulars" class="form-control">
                    <span class="error"></span>
                    <em>For example: Flight Ticket, Railway Ticket, etc </em> </div>
                  <div class="col-md-12 col-sm-12 col-xs-12  text-center"> <br>
                    <a class="gray-btn btnPrevious" >Back</a> &nbsp; <a class="green-btn btnNext" >Next</a> </div>
                </div>
              </div>
              <div class="tab-pane smooth-load" id="tab4">
                <div class="row">
                  <div class="col-md-12 col-sm-12 col-xs-12  text-center">
                    <div class="badge">4</div>
                    <h2>Interest on Home Loan </h2>
                    <div class="hr"></div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-4 col-sm-6 col-xs-12  form-group">
                    <label for="name"> Interest payable</label>
                    <input type="text" placeholder="Interest payable" name="home_loan_interest amount" class="form-control">
                    <span class="error"></span>
                  </div>
                  <div class="col-md-4 col-sm-6 col-xs-12  form-group">
                    <label for="name">Loan Provider (Name of Individual or Organization)</label>
                    <input type="text" placeholder="Name of Lender" name="loan_provider" class="form-control">
                    <span class="error"></span>
                  </div>
                  <div class="col-md-4 col-sm-6 col-xs-12  form-group">
                    <label for="name"> PAN of lender</label>
                    <input type="text" placeholder=" PAN of lender" name="lender_pan" class="form-control pan" pattern="[A-Z]{5}[0-9]{4}[A-Z]{1}">
                    <span class="error"></span>
                  </div>
                  <div class="col-md-8 col-sm-6 col-xs-12  form-group">
                    <label for="name">Address of Loan Provider</label>
                    <input type="text" placeholder="Address of Lender" name="lender_address" class="form-control">
                    <span class="error"></span>
                  </div>
                  <div class="col-md-4 col-sm-12 col-xs-12  form-group">
                    <label for="name">Evidence</label>
                    <input type="text" placeholder="Evidence" name="home_loan_particulars" class="form-control">
                    <span class="error"></span>
                  </div>
                  <div class="col-md-12 col-sm-12 col-xs-12  text-center"> <a class="gray-btn btnPrevious" >Back</a> &nbsp; <a class="green-btn btnNext" >Next</a> </div>
                </div>
              </div>
              <div class="tab-pane smooth-load" id="tab5">
                <div class="row">
                  <div class="col-md-12 col-sm-12 col-xs-12  text-center">
                    <div class="badge">5</div>
                    <h2>Deductions Under Chapter VI - A </h2>
                    <div class="hr"></div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12 col-sm-6 col-xs-12 ">
                    <h5><strong> 1. Deductions under Sec 80C </strong> </h5>
                  </div>
                </div>
                <div class="items">
                  <div class="row item item-1">
                    <div class="col-md-12 col-sm-12 col-xs-12 "> <span class="inlineDel delete">-</span> </div>
                    <div class="col-md-4 col-sm-12 col-xs-12  form-group">
                      <label for="name"> &nbsp;</label>
                      <select class="form-control" name="deduction_ec_type[1]">
                        <option>Life Insurance Premium</option>
                        <option>Investment in Tax Saving Fixed Deposit </option>
                        <option>Investment in Tax Saving Mutual Fund </option>
                        <option>Investment in PPF (Public Provident Fund) </option>
                        <option>Children's Tuition Fees </option>
                        <option>Principal repayment of Home Loan </option>
                        <option>Others </option>
                      </select>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12  form-group">
                      <label for="Amount">Amount</label>
                      <input type="text" placeholder="Amount" name="deduction_ec[1]" class="form-control amount">
                      <span class="error"></span>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12  form-group">
                      <label for="name">Evidence</label>
                      <input type="text" placeholder="Evidence" name="deduction_ec_particulars[1]" class="form-control">
                      <span class="error"></span>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12  text-right">
                      <button type="button" class="add-more addItem">+ Add</button>
                    </div>
                  </div>
                </div>
                <div class="clearfix"></div>
                <br>
                <div class="row">
                  <div class="col-md-12 col-sm-12 col-xs-12 ">
                    <h5><strong> 2. Other Deductions </strong> </h5>
                  </div>
                </div>
                <div class="items">
                  <div class="row item item-2">
                    <div class="col-md-12 col-sm-12 col-xs-12 "> <span class="inlineDel delete">-</span> </div>
                    <div class="col-md-4 col-sm-12 col-xs-12  form-group">
                      <label for="name"> &nbsp;</label>
                      <select class="form-control" name="deduction_eccd_type[1]">
                        <option>Sec 80 CCC - Deduction for contribution to Certain Pension Funds </option>
                        <option>Sec 80 CCD - Contribution to NPS </option>
                        <option>Sec 80D - Deduction for Health Insurance Premium </option>
                        <option>Sec 80DD - Deduction for Dependent Disabled </option>
                        <option>Sec 80E - Deduction for Interest on Eductaion Loan </option>
                        <option>Sec 80G - Deduction for Donations</option>
                      </select>
                      <span class="error"></span>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12  form-group">
                      <label for="Amount">Amount</label>
                      <input type="text" placeholder="Amount" class="form-control" name="deduction_eccd[1] amount" >
                      <span class="error"></span>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12  form-group">
                      <label for="name">Evidence</label>
                      <input type="text" placeholder="Evidence" class="form-control" name="deduction_eccd_particulars[1]">
                      <span class="error"></span>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12  text-right">
                      <button type="button" class="add-more addItem">+ Add</button>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12 col-sm-12 col-xs-12  text-center"> <a class="gray-btn btnPrevious" >Back</a> &nbsp;
                  <?= form_submit("submit","Submit",'class="green-btn" id="finish"'); ?>
                  </div>
                </div>
              </div>
            </div>
            <?php echo form_close() ?>
            <?php echo form_open(base_url('form12bb/generatepdf'),array('method'=>'post','id'=>'pdfForm','style'=>'display:none;')) ?>
          <?php echo form_hidden('html_content'); ?>
          <?php echo form_close() ?>
        </div>
      </div>
      <div class="pdf-result" style="display:none;">
        <div class="row">
          <div class="col-md-12"> </div>
          <div class="col-md-12">
            <div class="social-share">
              <h2 class="text-center"> Great, Here is your Form 12BB </h2>
              <iframe style="width: 100%; border: none; height: 1252px;"  id="iframe" name="iframe"></iframe>
              <h3>If you think your friends/network would find this useful, please share it with them &ndash; We'd really appreciate it.</h3>
              <p> <a href="https://api.addthis.com/oexchange/0.8/forward/twitter/offer?url=https%3A%2F%2Ftax2win.in%2Fplan-tax&pubid=ra-58866eb4a63f06e9&title=Plan%20your%20taxes%20%26%20File%20ITR%20with%20specialists%20to%20future-proof%20your%20Financial%20Life%20%23PlanTax&ct=1" target="_blank"><img src="https://cache.addthiscdn.com/icons/v3/thumbs/32x32/twitter.png" border="0" alt="Twitter"/></a> <a href="https://api.addthis.com/oexchange/0.8/forward/pinterest_share/offer?url=https%3A%2F%2Ftax2win.in%2Fplan-tax&pubid=ra-58866eb4a63f06e9&title=Plan%20your%20taxes%20%26%20File%20Income%20Tax%20Return%20with%20specialists%20to%20future-proof%20your%20Financial%20Life%20%23PlanTax&ct=1" target="_blank"><img src="https://cache.addthiscdn.com/icons/v3/thumbs/32x32/pinterest_share.png" border="0" alt="Pinterest"/></a> <a href="https://api.addthis.com/oexchange/0.8/forward/facebook/offer?url=https%3A%2F%2Ftax2win.in%2Fplan-tax&pubid=ra-58866eb4a63f06e9&title=Plan%20your%20taxes%20%26%20File%20Income%20Tax%20Return%20with%20specialists%20to%20future-proof%20your%20Financial%20Life%20%23PlanTax&ct=1" target="_blank"><img src="https://cache.addthiscdn.com/icons/v3/thumbs/32x32/facebook.png" border="0" alt="Facebook"/></a> <a href="https://api.addthis.com/oexchange/0.8/forward/linkedin/offer?url=https%3A%2F%2Ftax2win.in%2Fplan-tax&pubid=ra-58866eb4a63f06e9&title=Plan%20your%20taxes%20%26%20File%20Income%20Tax%20Return%20with%20specialists%20to%20future-proof%20your%20Financial%20Life%20%23PlanTax&ct=1" target="_blank"><img src="https://cache.addthiscdn.com/icons/v3/thumbs/32x32/linkedin.png" border="0" alt="LinkedIn"/></a> <a href="https://api.addthis.com/oexchange/0.8/forward/google_plusone_share/offer?url=https%3A%2F%2Ftax2win.in%2Fplan-tax&pubid=ra-58866eb4a63f06e9&title=Plan%20your%20taxes%20%26%20File%20Income%20Tax%20Return%20with%20specialists%20to%20future-proof%20your%20Financial%20Life%20%23PlanTax&ct=1" target="_blank"><img src="https://cache.addthiscdn.com/icons/v3/thumbs/32x32/google_plusone_share.png" border="0" alt="Google+"/></a> </p>
            </div>
          </div>
          <div class="col-md-12 text-right"><br>
            <a href="#" class="gray-btn pull-left" id="show-12bdiv">Back</a> <a onclick="generatepdf()" class="green-btn">Download Form 12BB</a> &nbsp; <a href="" class="gray-btn" onclick="printform()">Print Form</a> </div>
          <div class="col-md-12">
            <div class="form-pdf"> <img src="application/views/assets/img/form-12bb.jpg" alt="" class="img-responsive"> </div>
          </div>
        </div>
      </div>
    </div>
    <div class="container clearfix">
      <div class="global-faqs">
        <div class="wrapper">
          <h3 id="what-is-form12-bb"> What is Form-12 BB? </h3>
          <p>The Form 12BB is the form that you fill and give to your employer - not to the Income Tax Department - so that your employer can figure how much income tax is to be deducted from your monthly pay.</p>
          <p>Basically , it contain the details related to tax deductible expenses and investments which would be done by you during the current F.Y.</p>
          <p>Generally you fill out a form 12BB when you start a new job or in the beginning of the financial year. However, at the year end  you are supposed to submit the actual proofs/evidences against the tax exemptions ,investments and expenses already claimed by you in the form 12BB.</p>
          <p>Previously there was no standard format for declaring your tax deductible expenses and investments. With effect from 1st June 2016, Income tax department has introduced a standard format  of  Form 12BB.</p>
          <p>The form 12BB looks like this:</p>
          <img src="application/views/assets/img/form-12bb/form-12bb.jpg" alt="" class="img-responsive">
          <hr class="hr">
        </div>
        <div class="wrapper">
          <h3 id="the-purpose-of-form-12bb">What is the purpose of Form 12BB?</h3>
          <p>The Income Tax law gives powers to your employer to give you benefit of all the tax deductible expenses and investments made by you during the year at the time of deducting your income tax from your monthly paycheck.<br>
            However, just one condition is there ,  this benefit is provided to only those employees who submit their declaration / proofs of income tax deductions in Form 12BB are eligible for this benefit.Thus,Employees must complete this form so that their employers can know how much income tax needs to be deducted from their monthly paycheck and, in turn, how much money they will take home with each paycheck. </p>
          <div class="sub-well"> “Here's the thumb rule : The more tax deductible allowances, exemptions and investments you claim, the less income tax(TDS) your employer will deduct from your paycheck” </div>
          <p>Now, that we have understood the importance of Form 12BB, let’s understand Form 12BB a bit in detail.</p>
        </div>
        <div class="wrapper">
          <h3 id="what-is-the-format-of-form-12bb"> What is the format of Form 12BB?</h3>
          <p>The Income Tax Department has made a standard format of Form 12BB. It has to be prepared as per Rule 26C of the Income Tax Rules.You can download the form 12BB online in pdf or word format, print, fill and submit to your employer alongwith the required proofs.<br>
            Alternatively, you can fill Form 12BB online and either mail to your HR directly or print, sign and submit to your employer alongwith necessary proofs.</p>
			<p><strong>When to file Form 12BB?</strong><br>

Form 12BB is filed at the end of the Financial Year.
</p>
			
          <a href="https://emailer.tax2win.in/_/pdf/form12bb.pdf" target="_blank" class="green-btn"> Download fillable Form 12BB of Income Tax for 2017-18 Now!</a>
          <hr class="hr">
        </div>
        <div class="wrapper">
          <h3 id="information-are-required-be-filled-in-the-form-12bb"> What information are required to be filled in Form 12BB?</h3>
          <p>The details required to be reported in Form 12BB are:</p>
          <ul>
            <li>Your Personal Details - Name, Address, PAN Number , Position , Father’s Name</li>
            <li>Financial Year - Current Financial year is F.Y. 2017-18</li>
            <li>HRA : House Rent Allowance</li>
            <li>Leave travel concessions or assistance</li>
            <li>Interest on home loan </li>
            <li> Income Tax Deductions under Chapter VI-A of Income Tax</li>
            <li>Other Details like Date, Place etc</li>
          </ul>
          <hr class="hr">
        </div>
        <div class="wrapper">
          <h3 id="what-are-the-thing-to-be-done-before-filling-form-12bb"> What are the things to be done before  filling Form 12BB?</h3>
          <ul>
            <li>Study your CTC structure carefully to know, whether HRA or LTA are a part of your package.You can claim these allowances only when your employer explicitly makes them a part of your CTC structure.</li>
            <li>Go to your bank or download online your interest certificate and loan repayment schedule of your home loan and your bank account statement.</li>
            <li>Assemble all your receipts for all the income tax deductible expenses and investments done by you like Rent receipts, LIC premium receipt, tuition fees receipts, donation receipts etc.</li>
          </ul>
        </div>
        <div class="wrapper">
          <div class="well">
            <h3 id="how-to-fill-form-12bb"> How to fill Form 12BB?</h3>
            <p>Filling tax forms can be a nightmare for many of us. However, filing Form 12BB isn’t as scary as it might look. So,let’s just get down with it!</p>
            <p>Just follow these instructions to understand and fill the complete form to claim maximum tax benefit. Now Let's Discuss each part one by one in detail :</p>
            <h4>I. Personal Details :</h4>
            <p>This is the first section of Form 12BB, you need to mention your  :</p>
            <ul>
              <li>Full Name </li>
              <li>Address</li>
              <li>Permanent Account Number </li>
              <li>Financial year (Current Financial Year is F.Y. 2017-18)</li>
            </ul>
            <h4 id="hra">II. HRA(House Rent Allowance) : </h4>
            <img src="application/views/assets/img/form-12bb/hra-section.jpg">
            <p>For claiming HRA tax exemption, you need to submit the following details to your employer -</p>
            <ul>
              <li>Amount of Rent paid </li>
              <li>Name of your landlord</li>
              <li>Address of your landlord</li>
              <li>PAN No of your landlord in case the total amount of rent paid during the year exceeds Rs.1 lakh.</li>
            </ul>
            <p>In Addition, you also need to submit the proof for claiming HRA tax exemption .</p>
            <h5 id="proof-for-claiming-house-rent-allowance-tax-exemption">1. Evidence/Proof for  claiming House Rent Allowance tax exemption:</h5>
            <p>The proof for claiming HRA tax exemption are the monthly rent receipts. In many organisations, employer also ask for the rent agreement for allowing HRA tax exemption.</p>
            <h5 id="amount-of-tax-saving-on-house-rent-allowance">2. Amount of tax saving on House Rent Allowance(HRA):</h5>
            <p>This is the best tax saving avenue.Calculate your HRA tax exemption with our free <a href="https://tax2win.in/tax-tools/hra-calculator">HRA exemption calculator tool</a>.</p>
            <p><a href="https://tax2win.in/guide/rent-receipt">Read our complete guide on rent receipts</a> to know in detail how you can claim HRA tax exemption to save maximum tax</p>
            <h5 id="things-to-remember-when-claiming-hra-tax-exemption">3. Things to remember when claiming HRA tax exemption :</h5>
            <ul>
              <li>You can claim HRA tax exemption only when HRA is a part of your CTC.</li>
              <li>Incase, HRA is not a part of your CTC and you are living in a rented house you can claim tax benefit under section 80GG.</li>
              <li>Rent receipt is required only when your monthly rent exceeds Rs. 3,000. </li>
              <li>You can’t claim HRA if you are living in your own house.</li>
              <li>If you are paying rent to your parents, then ask them to show it as their income at the time of filing their Income Tax Return.</li>
              <li>Never submit fake rent receipts, this might land you in big trouble with the income tax authorities.</li>
              <li>Even if your employer do not ask for rent agreement, it is advisable to have a formal rent agreement printed on Rs. 500 stamp paper or as per the rate prevailing in your state for records.</li>
            </ul>
            <h4 id="leave-travel-concession">III. LTA(Leave Travel Concession/Allowance):</h4>
            <p>This allowance is one and the only allowance that helps saves tax only when you take a holiday.</p>
            <h5>1. Evidence/Proof for claiming LTA tax exemption:</h5>
            <p>To claim LTA, employees need to submit travel bills like boarding passes, flight tickets, invoice of travel agent, boarding pass etc. to employer. </p>
            <h5 id="amount-of-tax-saving-on-lta">2. Amount of tax saving on LTA :</h5>
            <p>This tax exemption is allowed only on actual travel cost to the extend specified in CTC.The fare is exempt as per the following conditions:</p>
            <div class="table-responsive">
              <table>
                <tbody>
                  <tr>
                    <th>Travel Mode</th>
                    <th>Exempt Amount</th>
                  </tr>
                  <tr>
                    <td>Air</td>
                    <td>Air fare of economy class in the National Carrier by the shortest route or the amount spent, whichever is less</td>
                  </tr>
                  <tr>
                    <td>Rail</td>
                    <td>Air-conditioned first class rail fare by the shortest route or the amount spent, whichever is less</td>
                  </tr>
                  <tr>
                    <td>Bus</td>
                    <td>First Class or deluxe class fare by the shortest route or the amount spent, whichever is less</td>
                  </tr>
                  <tr>
                    <td>Unrecognised public transport system</td>
                    <td>Air conditioned first class rail fare by shortest route or the amount spent, whichever is less</td>
                  </tr>
                </tbody>
              </table>
            </div>
            <h5 id="things-to-remember-when-claiming-lta-tax-exemption">3. Things to remember when claiming LTA tax exemption :</h5>
            <ul>
              <li>You can claim LTA only when it is a part of your CTC.</li>
              <li>You can claim LTA for yourself, your spouse, children,dependent parents and dependent brother and sister.</li>
              <li>It can be  claimed twice in a block of four years. The current block is 2014-2017.</li>
              <li>If you have claimed only 1 LTA in the previous block of 4 years, you can carry forward and utilise the 2nd LTA  but have to claim it in the first year itself of the next block.</li>
              <li>It is allowed for domestic travels and not for international travels.</li>
            </ul>
            <h4>IV. Deduction of Interest on Borrowing : </h4>
            <p>Deduction of interest on borrowings on home loan is allowed under section 24 of the income tax laws. You can claim deduction for interest on your home loan taken for construction,reconstruction, repair, purchase or renovation.</p>
            <p>The information needs to be filled in the Form 12BB are:</p>
            <ul>
              <li>Interest Payable/paid to the lender during the financial year</li>
              <li>Name of the lender from whom loan is taken</li>
              <li>Address of the lender </li>
              <li>PAN of the lender: Financial Institutions/Employer/Others, from whoever the loan is taken</li>
            </ul>
            <img src="application/views/assets/img/form-12bb/deduction-intrest-section.jpg">
            <h5 id="evidence-Proof-for-claiming-tax-exemption-for-interest-on-borrowing">1. Evidence/Proof for Claiming tax exemption for interest on borrowing:</h5>
            <p>Documents required to claim deduction u/s 24B on interest payment of home loan are:</p>
            <ul>
              <li>Statement / Certificate stating total EMI paid along with Interest and Principal components. </li>
              <li>Possession/construction completion certificate</li>
              <li>Self-declaration from employee whether house is self occupied or let out.</li>
            </ul>
            <h5 id="amount-of-saving-on-home-loan">2. Amount of saving on Home Loan :</h5>
            <strong>a. Tax benefits on payment of interest :</strong>
            <p> If you are paying interest on home loan, then quantum of deduction will depend on the type of house property.  Let’s discuss the same in detail.</p>
            <div class="sub-well">
              <p>Tip : Claiming deduction on interest payment shall result in a loss under head house property. This loss can be adjusted against income from other heads in the current year subject to the limit of Rs. 2 lakh.</p>
            </div>
            <h6>I. Tax benefit in case you have self-occupied property (SOP):</h6>
            <p>Maximum interest of Rs. 2, 00, 000 is allowable in case loan is taken for purchase or construction of your house. Such benefit shall reduced to Rs. 30, 000 in case loan is taken for repair/ reconstruction. Further, construction or purchase must be completed within 5 years from end of F.Y. in which loan is taken.</p>
            <h6>II. Tax benefits in case you have rented out(let-out) the property (LOP) :</h6>
            <p>The entire interest amount that you pay towards the loan is available as deduction in case of rented property. Such amount shall be deducted from the rental income for the year</p>
            <div class="sub-well">
              <p><strong>Note :</strong> However, from 1 April 2017 onwards i.e. F.Y. 2017-18, the maximum tax exemption of Rs.200,000/- can be taken for all type of houses(let-out/self-occupied). Incase, you have paid more than Rs.2,00,000 as interest on home loan taken for construction/purchase, then the remaining amount shall be allowed to be carried forward for set-off in subsequent years. </p>
            </div>
            <strong>b. Tax benefits on repayment of Principal Amount :</strong>
            <p>In both the cases whether there is self-occupied property or rented property, principal amount repayment is eligible to be claimed under Sec 80C of the income tax act. A maximum of Rs. 1.5 lakh can be claimed under Sec. 80C for the principal amount.(Max. limit of claiming all deductions under 80c is 1.5 lakh.So, plan accordingly.)</p>
            <h5> 3. Things to remember when claiming Interest on Home Loan tax exemption :</h5>
            <ul>
              <li>Incase, you have taken a home loan jointly then you can claim benefit of the interest deduction proportionately.</li>
              <li>If you have taken home loan from a lender other than bank i.e. your friends, relatives or any money lender the interest payment can be claimed as a deduction under section 24.Provided you take a certificate of interest from the person to whom you had paid interest.</li>
              <li>Where loan is taken from your friends, relatives or any money lender i.e. other than banks the  repayment of principal is not eligible for deduction under section 80C.</li>
            </ul>
            <p>This part of the form may take more time to finish if you are claiming maximum tax benefits .If you do not  have any deductions to make, you can then move on to last section. </p>
            <h4 id="deductions-under-chapter-vi-a">V. Deductions under Chapter VI-A</h4>
            <img src="application/views/assets/img/form-12bb/deduction-v-a-section.jpg">
            <p>Chapter VI-A covers income tax deduction under various sections like 80C, 80D (Medical insurance) 80G (Donation) etc. To claim deduction, evidence of investment made or expenditure incurred is required. 
              You might be wondering what kind of proofs are required to submitted to claim these deductions. Don’t Worry, we are here to help you. </p>
            <img src="application/views/assets/img/form-12bb/12bb-infographic.jpg" class="img-responsive border">
            <h4 id="verification">VI. Verification</h4>
            <img src="application/views/assets/img/form-12bb/verification.jpg">
            <p>The last section of the Form 12BB is the “verification”  of the information submitted in the Form 12BB. You need to just enter your name along with the name of your father/mother and the place(city in which you are filling the form) and date of filing the form and then sign the form. </p>
            <p>Done!<br>
              Easy isn’t it! </p>
            <p><strong>“In order to avoid tax deduction altogether, you’ll have to have no tax liability this year “</strong></p>
          </div>
          <h3 id="how-to-fill-out-form-12bb-when-you-changed-the-jobs"> How to Fill out Form 12BB when you changed the jobs?</h3>
          <p>If you have changed the jobs during a particular year then do not claim the maximum benefit of all the deductions with both the employers, otherwise your TDS deduction will not be correct and ultimately at the time of filing your Income Tax Return you will have to end up with paying tax to the Income Tax Department with applicable penal interest for late payment of taxes.</p>
          <hr class="hr">
          <h3 id="what-happens-if-you-accidentally-forget-to-give-your-employer-your-form-12bb"> What happens if you accidentally forget to give your employer your Form 12BB? </h3>
          <p>In case you forgot to submit form 12BB to your employer within the prescribed time, the employer will not be able to give you benefit of deductions and other tax exemptions. As a result, excess TDS will be deducted from your monthly salary.However, you can claim refund of such excess TDS while filing your income tax return.</p>
          <hr class="hr">
          <h3 id="how-to-claim-for-perquisites-that-are-not-covered-in-the-form-12bb"> How to claim for perquisites that are not covered in the Form 12BB ?</h3>
          <p>Perquisites are claimed in Form 12BA and not Form 12BB. Form 12BA is a statement showing particulars of perquisites, other fringe benefits or amenities and profits in lieu of salary with value thereof.</p>
          <hr class="hr">
          <h3 id="how-will-i-show-the-additional-investments-which-an-individual-misses-to-declare-in-the-form-12bb"> How will I show the additional investments which an individual misses to declare in the Form 12BB?</h3>
          <p>The additional investments which an individual had missed and not declared in Form 12BB can be claimed while filing return. Although, excess TDS will be deducted from his salary during the year, however he can claim refund of the same in his ITR.</p>
          <hr class="hr">
          <h3> I made declaration in January for making investments in March, But did not submit to my employer. What are the consequences of declaring investments but not submitting the proofs?</h3>
          <p>If you have not submitted  proof of some investments which you have declared in Form 12BB then those would not be considered by your employer. Resultantly , TDS would be calculated ignoring those proofless investments.</p>
          <br>
          <div class="sub-well">
            <h3 id="tips-on-navigating-tax-season"> After Form 12BB : Tips on Navigating Tax Season</h3>
            <p>Tax season can be a confusing and overwhelming time. With the number of different tax forms and sections out there, it’s difficult to keep track of them all. One way to ensure a stress-free tax season is by correctly filling out your form 12BB when you start a new job or in the beginning of the year. In addition, it’s a good idea to start your tax preparation early and stay on top of the process.<br>
              If you’re unsure whether you need to file your taxes, there’s an income threshold that helps determine whether you should file or not . This threshold is determined by factors like age, earned income, residential status.<br>
              If you’re struggling with income tax sections and provisions, not to worry. There are a lot of calculators out there to make the process clearer. You can use our  income tax calculator to estimate taxes you owe .<br>
              And keep in the mind that there are tax changes every year, so this year’s tax season may be different than last year’s. If this is hard to keep track, you can always seek a CA  for help. </p>
            <h3 id="conclusion"> Conclusion :</h3>
            <p>If you don't have enough TDS deducted, you will owe Income Tax Department at the time of income tax return filing. If you have excessive deduction, you will get an income tax refund. The key is to find the right balance.<br>
              Failing to turn in a Form 12BB is a big no-no. But you could run into other issues if you don’t include the right numbers on the form. If you don’t pay enough, you might owe additional taxes later on. If you make the mistake of paying taxes too much, you’ll get that money back in the form of a tax refund once you’ve filed and verified your income tax return. </p>
          </div>
        </div>
      </div>
    </div>
  </div>