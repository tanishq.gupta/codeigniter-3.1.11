<div class="banner">
    <div class="container">
      <div class="txt">
        <div class="row">
          <div class="col-md-4 col-sm-12 col-xs-12">
            <h1><strong>Generate Form<br>
              12BB</strong></h1>
          </div>
          <div class="col-md-8 col-sm-12 col-xs-12">
            <p>Form 12BB  helps in reducing tax liability for salaried employees. <br>
              Use this Form 12BB generator to declare investments, interest paid against Home Loan, <a class="link-d" href="https://tax2win.in/tax-tools/hra-calculator">Hour Rent Allowance</a>, Leave Travel Allowance and other tax saving deductions.</p>
          </div>
        </div>
      </div>
    </div>
  </div>