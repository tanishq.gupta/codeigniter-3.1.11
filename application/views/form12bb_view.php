
<?php
?><!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Free Form 12BB Generator[2017-18]. Claim your tax benefits to the maximum - Tax2win</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="copyright" content="Copyright (c) 2016 Tax2Win" />
<meta name="doc-type" content="Public" />
<meta name="robots" content="index, follow, all" />
<meta name="language" content="EN-US" />
<meta name="description" content="Tax2win - Generate Form 12BB online for FY 2017-18. Declaration tool for claiming maximum tax benefits using HRA, LTA, Tax Saving Deductions, Investments etc. ">
<meta name="distribution" content="Global" />
<meta name="last-modified" content="Sunday, 13 September 2016 10:33:43 GMT" />
<meta name="classification" content="Tax Filing" />
<link rel="stylesheet" href="application/views/assets/css/bootstrap.min.css">
<link rel="stylesheet" href="application/views/assets/css/style-new.css">
<link rel="stylesheet" href="application/views/assets/css/font-awesome/font-awesome.css">
</head>
<body>
<?php $this->load->view('subviews/form12bb_header'); ?>
<!-- banner -->
<section class="form-12bb">
  <?php $this->load->view('subviews/form12bb_banner'); ?>
  <?php $this->load->view('subviews/form12bb_form'); ?>
</section>
<div class="footer-new">
<?php $this->load->view('subviews/footer'); ?>
</div>
<script type="text/javascript" src="application/views/assets/js/jquery-1.12.4.min.js"></script> 
<script type="text/javascript" src="application/views/assets/js/bootstrap.min.js"></script> 
<script type="text/javascript" src="application/views/assets/js/form12bb.js"></script> 
</body>
</html>