<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Form12bb extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct(){
		 parent::__construct();
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->load->helper('url');
		$this->set_rules();
	}
	
	private function set_rules() {
		$this->form_validation->set_rules('name', 'Name', 'required|alpha_numeric_spaces|max_length[100]');
		$this->form_validation->set_rules('pan', 'Pan', 'required|regex_match[/[A-Z]{5}[0-9]{4}[A-Z]{1}/]', array(
			'regex_match' => 'Enter valid pan.'
		));
		$this->form_validation->set_rules('father_name', "Father's Name", 'required|alpha_numeric_spaces|max_length[100]');
		$this->form_validation->set_rules('emailid', 'Email Id', 'required|valid_email');
		$this->form_validation->set_rules('mobile', 'Mobile', 'required|integer|max_length[10]');
		$this->form_validation->set_rules('address', 'Address', 'required|alpha_numeric_spaces');
		$this->form_validation->set_rules('place', 'Place', 'required|alpha_numeric_spaces|max_length[100]');
		$this->form_validation->set_rules('hra_rent', 'Rent Amount', 'integer');
		$this->form_validation->set_rules('landlord_name', 'Landlord Name', 'alpha_numeric_spaces|max_length[100]');
		$this->form_validation->set_rules('hra_particulars', 'Rent Particulars', 'alpha_numeric_spaces|max_length[200]');
		$this->form_validation->set_rules('landlord_pan', 'Landlord Pan', 'regex_match[/[A-Z]{5}[0-9]{4}[A-Z]{1}/]', array(
			'regex_match' => 'Enter valid pan.'
		));
		$this->form_validation->set_rules('lta_amount', 'Leave Travel Allowance', 'integer|max_length[100000]');
		$this->form_validation->set_rules('lta_evidence', 'Leave Travel Particulars', 'alpha_numeric_spaces|max_length[200]');
		$this->form_validation->set_rules('home_loan_interest', 'Home Loan Interest', 'integer');
		$this->form_validation->set_rules('loan_provider', 'Loan Provider', 'alpha_numeric_spaces|max_length[100]');
		$this->form_validation->set_rules('lender_pan', 'Lender Pan', 'regex_match[/[A-Z]{5}[0-9]{4}[A-Z]{1}/]', array(
			'regex_match' => 'Enter valid pan.'
		));
		$this->form_validation->set_rules('lender_address', 'Lender Address', 'alpha_numeric_spaces');
		$this->form_validation->set_rules('home_loan_particulars', 'Loan Particulars', 'alpha_numeric_spaces|max_length[200]');
		$this->form_validation->set_rules('deduction_ec[]', 'Deduction80C Amount', 'integer');
		$this->form_validation->set_rules('deduction_ec_particulars[]', 'Deduction80C Particulars', 'alpha_numeric_spaces|max_length[200]');
	
		$this->form_validation->set_rules('deduction_eccd[]', 'Deduction Amount', 'integer');
		$this->form_validation->set_rules('deduction_eccd_particulars[]', 'Deduction Particulars', 'alpha_numeric_spaces|max_length[200]');

	}
	public function index()
	{
		$this->load->view('form12bb_view');
        
	}

	function processdata() {
		if ($this->input->is_ajax_request()) {
			if($this->form_validation->run() == FALSE){
				$errors = $this->form_validation->error_array();;
				$response = array('status' => 'error', 'data' => $errors);
				echo json_encode($response);
			}
			 else {
				$data = array(  
					'name'     => $this->input->post('name',true),  
					'pan'  => $this->input->post('pan',true),  
					'father_name'   => $this->input->post('father_name',true),  
					'emailid' => $this->input->post('emailid',true),
					'place' => $this->input->post('place',true),
					'mobile' => $this->input->post('mobile',true),
					'address' => $this->input->post('address',true),
					'hra_rent' => $this->input->post('hra_rent',true),
					'landlord_name' => $this->input->post('landlord_name',true),
					'hra_particulars' => $this->input->post('hra_particulars',true),
					'landlord_address' => $this->input->post('landlord_address',true),
					'landlord_pan' => $this->input->post('landlord_pan',true),
					'travel_expenses' => $this->input->post('travel_expenses',true),
					'travel_particulars' => $this->input->post('travel_particulars',true),
					'home_loan_interest' => $this->input->post('home_loan_interest',true),
					'loan_provider' => $this->input->post('loan_provider',true),
					'lender_pan' => $this->input->post('lender_pan',true),
					'lender_address' => $this->input->post('lender_address', true),
					'home_loan_particulars' => $this->input->post('home_loan_particulars', true),
					'deduction_ec_type' => json_encode($this->input->post('deduction_ec_type',true)),
					'deduction_ec' => json_encode($this->input->post('deduction_ec',true)),
					'deduction_ec_particulars' => json_encode($this->input->post('deduction_ec_particulars',true)),
					'deduction_eccd_type' => json_encode($this->input->post('deduction_eccd_type',true)),
					'deduction_eccd' => json_encode($this->input->post('deduction_eccd',true)),
					'deduction_eccd_particulars' => json_encode($this->input->post('deduction_eccd_particulars',true)),
					);  
				$this->savedata($data);
         }
		 }
	}

	function savedata($data) {
				$this->db->insert('form_12bb',$data);
				$view_data['values'] = $data; 
				$form_view = $this->load->view('subviews/form_12bb', $view_data,true);
				$csrf = $this->security->get_csrf_hash();
				$response = array('status' => 'success', 'data' => $form_view, 'csrf' => $csrf);
				echo json_encode($response);
    }

	public function generatePdf() {
        $this->load->library('pdf');
		$html = $this->input->post('html_content');
		$html = "<html>\n" . $html . "\n</html>";
		$this->pdf->setPaper('A4', 'portrait');
        $this->pdf->loadHtml($html);
        $this->pdf->render();
        $this->pdf->stream('output.pdf', ['Attachment' => true]);
    }
}
