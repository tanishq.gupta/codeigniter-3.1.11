<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Form_12bb extends CI_Migration {

        public function up()
        {
                $this->dbforge->add_field(array(
                        'name' => array(
                                'type' => 'TEXT',
                                'constraint' => '100',
                        ),
                        'pan' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '10',
                        ),
                        'father_name' => array(
                                'type' => 'TEXT',
                                'constraint' => '100',
                                'null' => TRUE,
                        ),
                        'emailid' => array(
                            'type' => 'TEXT',
                            'constraint' => '100',
                            'null' => TRUE,
                        ),
                        'place' => array(
                            'type' => 'VARCHAR',
                            'constraint' => '100',
                            'null' => TRUE,
                        ),
                        'mobile' => array(
                            'type' => 'TEXT',
                            'constraint' => '10',
                            'null' => TRUE,
                        ),
                        'address' => array(
                            'type' => 'VARCHAR',
                            'constraint' => '1000',
                            'null' => TRUE,
                        ),
                        'hra_rent' => array(
                            'type' => 'INT',
                            'constraint' => '255',
                            'null' => TRUE,
                            ),
                        'landlord_name' => array(
                            'type' => 'TEXT',
                            'null' => TRUE,
                            'constraint' => '100',
                            ),
                        'hra_particulars' => array(
                            'type' => 'VARCHAR',
                            'null' => TRUE,
                            'constraint' => '200',
                            ),
                        'landlord_address' => array(
                            'type' => 'VARCHAR',
                            'null' => TRUE,
                            'constraint' => '1000',
                            ),
                        'landlord_pan' => array(
                            'type' => 'VARCHAR',
                            'null' => TRUE,
                            'constraint' => '10',
                            ),
                        'travel_expenses' => array(
                            'type' => 'INT',
                            'null' => TRUE,
                            'constraint' => '255',
                            ),
                        'travel_particulars' => array(
                            'type' => 'VARCHAR',
                            'null' => TRUE,
                            'constraint' => '1000',
                            ),
                        'home_loan_interest' => array(
                            'type' => 'INT',
                            'null' => TRUE,
                            'constraint' => '255',
                            ),
                        'loan_provider' => array(
                            'type' => 'TEXT',
                            'null' => TRUE,
                            'constraint' => '1000',
                            ),
                        'lender_pan' => array(
                            'type' => 'INT',
                            'constraint' => '10',
                            ),
                        'lender_address' => array(
                            'type' => 'VARCHAR',
                            'null' => TRUE,
                            'constraint' => '1000',
                            ),
                        'home_loan_particulars' => array(
                            'type' => 'VARCHAR',
                            'null' => TRUE,
                            'constraint' => '200',
                            ),
                        'deduction_ec_type' => array(
                            'type' => 'VARCHAR',
                            'null' => TRUE,
                            'constraint' => '1000',
                            ),
                        'deduction_ec' => array(
                            'type' => 'INT',
                            'null' => TRUE,
                            'constraint' => '1000',
                            ),
                        'deduction_ec_particulars' => array(
                            'type' => 'VARCHAR',
                            'null' => TRUE,
                            'constraint' => '200',
                            ),
                        'deduction_eccd_type' => array(
                            'type' => 'VARCHAR',
                            'null' => TRUE,
                            'constraint' => '1000',
                            ),
                        'deduction_eccd' => array(
                            'type' => 'INT',
                            'null' => TRUE,
                            'constraint' => '1000',
                            ),
                        'deduction_eccd_particulars' => array(
                            'type' => 'VARCHAR',
                            'null' => TRUE,
                            'constraint' => '200',
                            ),
                ));
                $this->dbforge->create_table('form_12bb');
        }

        public function down()
        {
                $this->dbforge->drop_table('form_12bb');
        }
}